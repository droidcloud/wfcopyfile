/***
echo "check the flags..."
if [ ! -f /mnt/sdcard/external_sdcard/resource/wow.flag ]; then
    echo "input resource sdcard is not avaible, please check!"
    exit
fi

echo "begin to clean the dirty folder..."
rm -r /mnt/sdcard/APK/*
rm -r /mnt/sdcard/EBook/*
rm -r /mnt/sdcard/Favorite/*
rm -r /mnt/sdcard/Music/*
rm -r /mnt/sdcard/Video/*

echo "begin to copy files..."
cp -rf /mnt/sdcard/external_sdcard/resources/APK/*   /mnt/sdcard/APK/
cp -rf /mnt/sdcard/external_sdcard/resources/EBook/*   /mnt/sdcard/EBook/
cp -rf /mnt/sdcard/external_sdcard/resources/Favorite/*   /mnt/sdcard/Favorite/
cp -rf /mnt/sdcard/external_sdcard/resources/Music/*   /mnt/sdcard/Music/
cp -rf /mnt/sdcard/external_sdcard/resources/Video/*   /mnt/sdcard/Video/

sync

echo "Done, you can remove the external sdcard!"

mkdir APk
touch APK/1.apk
touch APK/2.apk
touch APK/3.apk
mkdir EBook
touch EBook/1.txt
touch EBook/2.txt
touch EBook/3.txc
mkdir Favorite
touch Favorite/1
touch Favorite/2
touch Favorite/3
touch Favorite/5
mkdir Music
touch Music/1.mp3
touch Music/2.mp3
touch Music/3.mp3
mkdir Video
touch Video/1.ogg
touch Video/2.mbr
touch Video/4.mp4

***/
package com.android.test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class TestActivity extends Activity {
    public static String[] destPath = {
            "/mnt/sdcard/Video/",
            "/mnt/sdcard/APK/",
            "/mnt/sdcard/EBook/",
            "/mnt/sdcard/Music/",
            "/mnt/sdcard/Favorite/"
    };

    public static String[] srcPath = {
            "/mnt/sdcard/external_sdcard/resources/Video/",
    	    "/mnt/sdcard/external_sdcard/resources/APK/",
            "/mnt/sdcard/external_sdcard/resources/EBook/",
            "/mnt/sdcard/external_sdcard/resources/Music/",
            "/mnt/sdcard/external_sdcard/resources/Favorite/"
    };
    private Button copyButton = null;
    private Button uninstallButton = null;
    private TextView logTextView = null;
    private ProgressDialog mProgressDialog;
    public static final int DIALOG_PROGRESS = 0;
    private StringBuilder logs = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        // initial
        copyButton = (Button) findViewById(R.id.copy_button);
        uninstallButton = (Button) findViewById(R.id.uninstall_button);
        logTextView = (TextView) findViewById(R.id.status_log);

        copyButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
            	logs = new StringBuilder();
                
                new AsyncTask<Void, String, Void>() {
                    @Override
                    protected Void doInBackground(Void... args) {
                        for (int i = 0; i < srcPath.length; i++) {
                            copyDirectory(srcPath[i], destPath[i]);
                            String log = srcPath[i]+" to "+destPath[i]+" done\n";
                            publishProgress(log);
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        super.onPostExecute(result);
                        if(mProgressDialog.isShowing()){
                            dismissDialog(DIALOG_PROGRESS);
                        	logs.append("Copy done\n");
                            logTextView.setText(logs.toString());
                            
                            uninstallPkg(getPackageName());
                        }
                    }

                    @Override
                    protected void onPreExecute() {
                        super.onPreExecute();
                        showDialog(DIALOG_PROGRESS);
                    	logs.append("Start to copy\n");
                        logTextView.setText(logs.toString());
                    }

                    @Override
                    protected void onProgressUpdate(String... values) {
                        super.onProgressUpdate(values);
                        logs.append(values[0]);
                        logTextView.setText(logs.toString());
                    }
                }.execute();
            }
        });

        uninstallButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                uninstallPkg(getPackageName());
            }
        });
    }


    @Override
    protected Dialog onCreateDialog(int id, Bundle args) {
        switch (id) {  
            case DIALOG_PROGRESS: //we set this to 0  
                mProgressDialog = new ProgressDialog(this);
                mProgressDialog.setMessage(getResources().getText(R.string.copying));
                mProgressDialog.setCancelable(false);
                mProgressDialog.setIndeterminate(true);
                //mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);  
                mProgressDialog.show();  
                return mProgressDialog;  
            default:  
                return null;  
        }  
    }


    private void uninstallPkg(String packageName) {
        Uri packageURI = Uri.parse("package:" + packageName);
        Intent uninstallIntent = new Intent(Intent.ACTION_DELETE, packageURI);
        startActivity(uninstallIntent);
    }

    private void deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                deleteDir(new File(dir, children[i]));
            }
        }
        dir.delete();
    }
    

    public void copyDirectory(String srcDirName, String destDirName) {
        File srcDir = new File(srcDirName);
        if (!srcDir.exists()) {
            return;
        } else if (!srcDir.isDirectory()) {
            return;
        }

        File destDir = new File(destDirName);
        if (destDir.exists()) {
            deleteDir(destDir);
        }
        
        if (!destDir.mkdirs()) {
            return;
        }

        File[] files = srcDir.listFiles();
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                copyFile(files[i].getAbsolutePath(), destDirName + files[i].getName());
            } else if (files[i].isDirectory()) {
                copyDirectory(files[i].getAbsolutePath(), destDirName + files[i].getName());
            }
        }
    }

    public boolean copyFile(String srcFileName, String destFileName) {
        File srcFile = new File(srcFileName);
        File destFile = new File(destFileName);
        
        if (destFile.exists()) {
            new File(destFileName).delete();
        }

        int byteread = 0;
        InputStream in = null;
        OutputStream out = null;

        try {
            in = new FileInputStream(srcFile);
            out = new FileOutputStream(destFile);
            byte[] buffer = new byte[1024];

            while ((byteread = in.read(buffer)) != -1) {
                out.write(buffer, 0, byteread);
            }
            return true;
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (out != null)
                    out.close();
                if (in != null)
                    in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
